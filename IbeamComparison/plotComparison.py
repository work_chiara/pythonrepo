#!/usr/bin/python

from datetime import datetime

from array import array

from ROOT import TCanvas, TLegend, TGraph, TH1F

from columnFile import columnFile


class plotComparison:

    def __init__(self):

        self.dia_FWD0 = columnFile("FWD0")
        self.dia_FWD1 = columnFile("FWD1")
        self.dia_FWD2 = columnFile("FWD2")
        self.dia_FWD3 = columnFile("FWD3")
        self.ler = columnFile("ler")
        self.her = columnFile("her")
        self.dia_FWD2Exp_her = columnFile("dia_FWD2Exp_her")

    def makePlots(self):
        self.makeSpecificPlot("ler_vs_FW0.root", self.ler.returnVect(), self.dia_FWD0.returnVect())
        self.makeSpecificPlot("ler_vs_FW1.root", self.ler.returnVect(), self.dia_FWD1.returnVect())
        self.makeSpecificPlot("ler_vs_FW2.root", self.ler.returnVect(), self.dia_FWD2.returnVect())
        self.makeSpecificPlot("ler_vs_FW3.root", self.ler.returnVect(), self.dia_FWD3.returnVect())
        self.makeSpecificPlot("her_vs_FW0.root", self.her.returnVect(), self.dia_FWD0.returnVect())
        self.makeSpecificPlot("her_vs_FW1.root", self.her.returnVect(), self.dia_FWD1.returnVect())
        self.makeSpecificPlot("her_vs_FW2.root", self.her.returnVect(), self.dia_FWD2.returnVect())
        self.makeSpecificPlot("her_vs_FW3.root", self.her.returnVect(), self.dia_FWD3.returnVect())


    def makeSpecificPlot(self, fileName, vec_current_ler, vec_doseRate):
        c1 = TCanvas('c1', 'FWD', 900, 600)
        gr = TGraph(len(vec_current_ler), vec_current_ler, vec_doseRate)
        #gr.SetLineColor(2)
        #gr.SetLineWidth(4)
        gr.SetMarkerColor(4)
        gr.SetMarkerStyle(21)
        gr.SetMarkerSize(0.35)
        gr.GetXaxis().SetTitle("current [mA]")
        gr.GetYaxis().SetTitle('dose rate [mRad/s]')
        gr.Draw("AP")
        c1.SaveAs(fileName)


    def readFile(self, filename):
        print filename
        infile = open(filename, "r")
        for line in infile:
            parts = line.split()
            data = parts[0]
            hour = parts[1]
            herCurrentString = parts[2] #il 24 ho prima HER ma il 30 prima LER...
            lerCurrentString = parts[3]
            doseRate0String = parts[4]
            doseRate1String = parts[5]
            doseRate2String = parts[6]
            doseRate3String = parts[7]
            herCurrent = 0
            lerCurrent = 0
            doseRate0 = 0
            doseRate1 = 0
            doseRate2 = 0
            doseRate3 = 0
            try:
                doseRate0 = float(doseRate0String)
                doseRate1 = float(doseRate1String)
                doseRate2 = float(doseRate2String)
                doseRate3 = float(doseRate3String)
                herCurrent = float(herCurrentString)
                lerCurrent = float(lerCurrentString)
                space = " "
                dataTime = data + space + hour[0:15]
                timeStamp = datetime.strptime(dataTime, "%Y-%m-%d %H:%M:%S.%f")
            except:
                print "Error on format:" + line
                raise
            if timeStamp > datetime.strptime('2018-04-24 01:00:00.00', "%Y-%m-%d %H:%M:%S.%f") \
                    and timeStamp < datetime.strptime('2018-04-24 03:30:00.00', "%Y-%m-%d %H:%M:%S.%f"): # only HER
                self.dia_FWD0.add(doseRate0, timeStamp)
                self.dia_FWD1.add(doseRate1, timeStamp)
                self.dia_FWD2.add(doseRate2, timeStamp)
                self.dia_FWD3.add(doseRate3, timeStamp)
                self.ler.add(lerCurrent, timeStamp)
                self.her.add(herCurrent, timeStamp)

        infile.close()


def main():
    compare = plotComparison()

    fileList = ['/Users/chiaralalicata/HD/file_BEAST_phase2/Ibeam/BKGstudy24-04/CurrentsDoseRate_24_04.txt']
    for file in fileList:
        compare.readFile(file)

    compare.makePlots()


if __name__ == "__main__":
    main()
