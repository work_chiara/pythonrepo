from array import array

from datetime import datetime

from ROOT import TGraph, TH1F

class columnFile:
    def __init__(self, name):
        self.name = name
        self.doseRateOrCurr_vec, self.timeStamp_vec = array('d'), list()

    def add(self, doseRateOrCurr, timeStamp):
        self.doseRateOrCurr_vec.append(doseRateOrCurr)
        self.timeStamp_vec.append(timeStamp)

    def returnVect(self):
        return self.doseRateOrCurr_vec

    def returnTime(self):
        return self.timeStamp_vec
