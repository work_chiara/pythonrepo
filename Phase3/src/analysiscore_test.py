import os
import unittest

from analysisCore import *


class TestAnalysisCore(unittest.TestCase):

    def test_getIntegratedDose(self):
        conf = Configuration("")
        conf.configurationToBeUsed[DEFINITION] = {
            'DCU1ch0': "SVD_BW_60",
            'DCU1ch1': "SVD_BW_120",
            'DCU1ch2': "SVD_BW_240",
            'DCU1ch3': "SVD_BW_300"}
        conf.configurationToBeUsed[STUDY_START] = "2019-03-06 20:13:12"
        conf.configurationToBeUsed[STUDY_END] = "2019-03-06 20:13:14"

        analysis = AnalysisCore(conf, "")
        analysis.readFile("/Users/lalicata/LAVORO/pythonrepo/Phase3/resources/DCU010Hz_small.log")
        analysis.evaluatePedestalOn("2019-03-06 20:13:20", "2019-03-06 20:13:21")

        integratedDose = analysis.getIntegratedDose("DCU1ch0")
        self.assertAlmostEqual(540.9318115, integratedDose)
        integratedDose = analysis.getIntegratedDose("DCU1ch1")
        self.assertAlmostEqual(181.2886231, integratedDose)
        integratedDose = analysis.getIntegratedDose("DCU1ch2")
        self.assertAlmostEqual(178.6457946, integratedDose)
        integratedDose = analysis.getIntegratedDose("DCU1ch3")
        self.assertAlmostEqual(846.9072998, integratedDose)

    def test_createDoseRateVSTimePlot(self):
        conf = Configuration("")
        conf.configurationToBeUsed[DEFINITION] = {
            'DCU1ch0': "SVD_BW_60",
            'DCU1ch1': "SVD_BW_120",
            'DCU1ch2': "SVD_BW_240",
            'DCU1ch3': "SVD_BW_300"}

        analysis = AnalysisCore(conf, "")
        analysis.readFile("/Users/lalicata/LAVORO/pythonrepo/Phase3/resources/DCU010Hz.log")
        analysis.diamondsMap["DCU1ch0"].setPedestal(0)

        analysis.createDoseRateVSTimePlot("DCU1ch0", "2019-03-06 20:13:12", "2019-03-06 20:13:14")
        # TODO cambiare nome file con il nome vero del diamante
        self.assertTrue(os.path.isfile('./SVD_BW_60_doseRateVsTime.root'))

    def test_pedestalEvaluation(self):
        conf = Configuration("")
        conf.configurationToBeUsed[DEFINITION] = {
            'DCU1ch0': "SVD_BW_60",
            'DCU1ch1': "SVD_BW_120",
            'DCU1ch2': "SVD_BW_240",
            'DCU1ch3': "SVD_BW_300"}
        analysis = AnalysisCore(conf, "")
        analysis.readFile("/Users/lalicata/LAVORO/pythonrepo/Phase3/resources/DCU010Hz.log")

        pedestalVec = analysis.evaluatePedestalOn("2019-03-06 20:13:12", "2019-03-06 20:13:14")

        self.assertAlmostEqual(2686.39312261538, pedestalVec['SVD_BW_60'])
        self.assertAlmostEqual(906.294663653846, pedestalVec['SVD_BW_120'])
        self.assertAlmostEqual(892.831728307692, pedestalVec['SVD_BW_240'])
        self.assertAlmostEqual(4881.06940165385, pedestalVec['SVD_BW_300'])

    def test_integratedDoseWithPedestal(self):
        conf = Configuration("")
        conf.configurationToBeUsed[DEFINITION] = {
            'DCU1ch0': "SVD_BW_60",
            'DCU1ch1': "SVD_BW_120",
            'DCU1ch2': "SVD_BW_240",
            'DCU1ch3': "SVD_BW_300"}
        conf.configurationToBeUsed[STUDY_START] = "2019-03-06 20:13:12"
        conf.configurationToBeUsed[STUDY_END] = "2019-03-06 20:13:14"

        analysis = AnalysisCore(conf, "")
        analysis.readFile("/Users/lalicata/LAVORO/pythonrepo/Phase3/resources/DCU010Hz_small.log")
        analysis.evaluatePedestalOn("2019-03-06 20:13:12", "2019-03-06 20:13:14")

        integratedDose = analysis.getIntegratedDose("DCU1ch0")
        self.assertAlmostEqual(1.94533689999998, integratedDose)

    def test_generateConfigurationJson(self):
        diamondsDefinition1 = {'DCU1ch0': "SVD_BW_60",
                               'DCU1ch1': "SVD_BW_120",
                               'DCU1ch2': "SVD_BW_240",
                               'DCU1ch3': "SVD_BW_300"}
        dataFiles1 = ['file1', 'file2', 'file3']
        configuration1 = {"definition": diamondsDefinition1, "dataFiles": dataFiles1,
                          "pedistalInit": "2019-03-06 20:13:12", "pedistalEnd": "2019-03-06 20:13:14",
                          "studyInit": "2019-03-06 20:13:12", "studylEnd": "2019-03-06 20:13:14"}

        diamondsDefinition2 = {'DCU1ch0': "SVD_BW_60",
                               'DCU1ch1': "SVD_BW_120",
                               'DCU1ch2': "SVD_BW_240",
                               'DCU1ch3': "SVD_BW_300"}

        dataFiles2 = ['file1', 'file2', 'file3']
        configuration2 = {"definition": diamondsDefinition2, "dataFiles": dataFiles2,
                          "pedistalInit": "2019-03-06 20:13:12", "pedistalEnd": "2019-03-06 20:13:14",
                          "studyInit": "2019-03-06 20:13:12", "studylEnd": "2019-03-06 20:13:14"}

        configurationFile = {"configuration1": configuration1,
                             "configuration2": configuration2,
                             "configurationToBeUsed": "configuration2"
                             }

        # Create a file in JSON
        with open('data.txt', 'w') as outfile:
            json.dump(configurationFile, outfile, sort_keys=True, indent=4, separators=(',', ': '))


if __name__ == '__main__':
    unittest.main()
