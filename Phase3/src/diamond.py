from array import array
from datetime import datetime

from ROOT import TGraph, TH1F, TCanvas


class Diamond:
    def __init__(self, name):
        self.label = name
        self.pedestal = "None"
        self.doseRate_vec = array('d')
        self.timeStamp_vec = list()
        self.millisecond_vec = array('d')

    def getLabel(self):
        return self.label

    def add(self, doseRate, timeStamp, milliseconds):
        self.doseRate_vec.append(doseRate)
        self.timeStamp_vec.append(timeStamp)
        self.millisecond_vec.append(milliseconds)

    def getIntegratedDose(self, startTime, endTime):
        if (self.pedestal == "None"):
            raise Exception('You have to call pedestalEvaluation or setPedestal method first!')

        timeStamp_subSet = list()
        milliseconds_double = array('d')
        doseRate_double = array('d')
        startTimePlot = datetime.strptime(startTime, "%Y-%m-%d %H:%M:%S")
        endTimePlot = datetime.strptime(endTime, "%Y-%m-%d %H:%M:%S")
        for index in range(len(self.timeStamp_vec)):
            if (self.timeStamp_vec[index] <= endTimePlot and self.timeStamp_vec[index] >= startTimePlot):
                timeStamp_subSet.append(self.timeStamp_vec[index])
                doseRate_double.append(self.doseRate_vec[index])
                milliseconds_double.append(self.millisecond_vec[index])

        integrateDose = 0
        for i in range(len(doseRate_double) - 1):
            diffTimeStamp = timeStamp_subSet[i + 1] - timeStamp_subSet[i]
            diffMilliseconds = milliseconds_double[i + 1] - milliseconds_double[i]
            seconds = diffTimeStamp.total_seconds()
            totalMilliseconds = seconds + diffMilliseconds / 1000
            integrateDose = integrateDose + (doseRate_double[i] - self.pedestal) * totalMilliseconds
        return integrateDose

    def plot(self, startTime, endTime):
        if (self.pedestal == "None"):
            raise Exception('You have to call pedestalEvaluation or setPedestal method first!')

        timeStamp_double = array('d')
        doseRate_double = array('d')
        startTimePlot = datetime.strptime(startTime, "%Y-%m-%d %H:%M:%S")
        endTimePlot = datetime.strptime(endTime, "%Y-%m-%d %H:%M:%S")
        for index in range(len(self.timeStamp_vec)):
            if (self.timeStamp_vec[index] <= endTimePlot and self.timeStamp_vec[index] >= startTimePlot):
                timeStamp_double.append(float(self.timeStamp_vec[index].strftime("%s")))
                doseRate_double.append(self.doseRate_vec[index] - self.pedestal)
        gr = TGraph(len(timeStamp_double), timeStamp_double, doseRate_double)
        gr.SetLineColor(2)
        gr.SetLineWidth(4)
        gr.SetMarkerColor(4)
        gr.SetMarkerStyle(21)
        gr.SetMarkerSize(0.35)
        gr.SetTitle('')
        gr.GetXaxis().SetTimeDisplay(1)
        gr.GetXaxis().SetNdivisions(5, 5, 0, 0)
        # gr.GetXaxis().SetTimeFormat("#splitline{%m-%d}{h%H}%F1970-01-01 00:00:00")
        gr.GetXaxis().SetTimeFormat("#splitline{}{%H:%M:%S}%F1970-01-01 00:00:00 ")
        gr.GetXaxis().SetLabelOffset(0.02)
        gr.GetXaxis().SetTitleOffset(1.4)
        gr.GetYaxis().SetTitleOffset(1.4)
        gr.GetXaxis().SetTitle("date [h-m]")
        gr.GetYaxis().SetTitle('dose rate [mRad/s]')
        return gr

    def pedestalEvaluation(self, initDate, endDate):
        startTime = datetime.strptime(initDate, "%Y-%m-%d %H:%M:%S")
        endTime = datetime.strptime(endDate, "%Y-%m-%d %H:%M:%S")
        max, min = self.getMinMax(endTime, startTime)
        c13 = TCanvas('c13', ' histo', 900, 600)
        histo = TH1F("histo", "histo", 10, min, max)

        elements = pedestal = 0
        for i in range(len(self.doseRate_vec)):
            if self.timeStamp_vec[i] <= endTime and self.timeStamp_vec[i] >= startTime:
                histo.Fill(self.doseRate_vec[i])
                elements = elements + 1
                pedestal = pedestal + self.doseRate_vec[i]
        if elements != 0:
            pedestal = pedestal / elements
        histo.Draw()
        c13.SaveAs("histo_" + self.label + ".root")
        self.pedestal = pedestal
        return pedestal

    def getMinMax(self, endTime, startTime):
        min, max = 1000000, -1000000
        for i in range(len(self.doseRate_vec)):
            if self.timeStamp_vec[i] <= endTime and self.timeStamp_vec[i] >= startTime:
                if (self.doseRate_vec[i] < min):
                    min = self.doseRate_vec[i]
                if (self.doseRate_vec[i] > max):
                    max = self.doseRate_vec[i]
        return max, min

    def setPedestal(self, pedestal):
        self.pedestal = pedestal
