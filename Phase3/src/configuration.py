import json

CONFIGURATION_TO_BE_USED = 'configurationToBeUsed'
DATA_FILES = 'dataFiles'
DEFINITION = 'definition'
PEDISTAL_START = 'pedistalStart'
PEDISTAL_END = 'pedistalEnd'
STUDY_START = 'studyStart'
STUDY_END = 'studyEnd'


class Configuration(object):
    data = {}
    configurationToBeUsed = {}

    def __init__(self, path):
        self.path = path

    def load(self):
        with open(self.path) as json_file:
            self.data = json.load(json_file)
            configurationToBeUsed = self.data[CONFIGURATION_TO_BE_USED]
            self.configurationToBeUsed = self.data[configurationToBeUsed]

    def getPedestalStart(self):
        return self.configurationToBeUsed[PEDISTAL_START]

    def getDiamondsLabels(self):
        return self.configurationToBeUsed[DEFINITION]

    def getDataFiles(self):
        return self.configurationToBeUsed[DATA_FILES]

    def getPedestalEnd(self):
        return self.configurationToBeUsed[PEDISTAL_END]

    def getStudyStart(self):
        return self.configurationToBeUsed[STUDY_START]

    def getStudyEnd(self):
        return self.configurationToBeUsed[STUDY_END]
