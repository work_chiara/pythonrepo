import re
import sys

from ROOT import TCanvas

from configuration import *
from diamond import *


class AnalysisCore():
    pedestalTimeTable = {}
    diamondsMap = {}

    def __init__(self, configuration, pedestalFile):
        self.configuration = configuration
        self.pedestalFile = pedestalFile
        self.loadPedestals(pedestalFile)

        diamonds = configuration.getDiamondsLabels()
        for phisicalAddress, label in diamonds.iteritems():
            self.diamondsMap[phisicalAddress] = Diamond(label)

    def readFile(self, dataFile):
        print dataFile
        infile = open(dataFile, "r")
        for line in infile:
            parts = re.compile("\[|\;\s|\s+|\]\s|::|=").split(line)
            data = parts[1]
            hour = parts[2]
            dcuNumber = parts[6]
            milliseconds = parts[10]
            # TODO: da controllare i channel name
            doseRateCh0 = parts[12]
            doseRateCh1 = parts[14]
            doseRateCh2 = parts[16]
            doseRateCh3 = parts[18]

            space = " "
            dataTime = data + space + hour
            timeStamp = datetime.strptime(dataTime, "%Y-%m-%d %H:%M:%S")

            diamond0 = self.getDiamond(dcuNumber + "ch0")
            diamond1 = self.getDiamond(dcuNumber + "ch1")
            diamond2 = self.getDiamond(dcuNumber + "ch2")
            diamond3 = self.getDiamond(dcuNumber + "ch3")

            diamond0.add(float(doseRateCh0), timeStamp, float(milliseconds))
            diamond1.add(float(doseRateCh1), timeStamp, float(milliseconds))
            diamond2.add(float(doseRateCh2), timeStamp, float(milliseconds))
            diamond3.add(float(doseRateCh3), timeStamp, float(milliseconds))

    def getIntegratedDose(self, phisicalAddress):
        diamond = self.getDiamond(phisicalAddress)
        integratedDose = diamond.getIntegratedDose(self.configuration.getStudyStart(), self.configuration.getStudyEnd())
        return integratedDose

    def getDiamond(self, dcuAndChannel):
        return self.diamondsMap.get(dcuAndChannel)

    def createDoseRateVSTimePlot(self, physicalAddress, startTime, endTime):
        diamond = self.getDiamond(physicalAddress)
        name = "%s_doseRateVsTime"
        c9 = TCanvas('c9', name % (diamond.getLabel()), 900, 600)
        plot = diamond.plot(startTime, endTime)
        plot.Draw()
        c9.SaveAs(name % (diamond.getLabel()) + ".root")
        return 0

    def createCorrelationPlot(self, firstDcu, firstChannel, secondDcu, secondChannel, startTime, endTime):
        return 0

    def evaluatePedestalOn(self, startTime, endTime):
        pedestals = {}
        for diamond in self.diamondsMap.itervalues():
            pedestals.update({diamond.label: diamond.pedestalEvaluation(startTime, endTime)})
        self.pedestalTimeTable.update({startTime: pedestals})
        self.savePedestals()
        return pedestals

    def loadPedestals(self, pedestalFile):
        # TODO ricarica file pedestals
        pass

    def savePedestals(self):
        # TODO salva su file i pedestals
        pass


def main():
    configurationFile = sys.argv[1]
    configuration = Configuration(configurationFile)
    configuration.load()
    analisysCore = AnalysisCore(configuration, "")

    fileList = configuration.getDataFiles()
    for file in fileList:
        analisysCore.readFile(file)

    analisysCore.evaluatePedestalOn(configuration.getPedestalStart(), configuration.getPedestalEnd())

    label = "%s - Integrated dose = %s"
    for address, diamondLabel in configuration.getDiamondsLabels().iteritems():
        print label % (diamondLabel, analisysCore.getIntegratedDose(address))
        analisysCore.createDoseRateVSTimePlot(address, configuration.getStudyStart(), configuration.getStudyEnd())

if __name__ == "__main__":
    main()
