from array import array

from ROOT import TGraph

INTEGRATION_TIME_WINDOW = 100  # 1 ms
TIME_BETWEEN_CONSECUTIVE_DATA = 0.00001  # time interval between two consecutive data elements. This is because the memory are read at 100kHz
SAMPLING_FREQUENCY = 100000


class Diamond:

    def __init__(self, name):
        self.name = name
        self.adcVec = array('d')
        self.timeVec = array('d')

        self.currentToDoseRate = 0
        self.adcToCurrent = 0
        self.pedestal = 0

    def setCurrToDoseFactor(self, currentToDoseRate):
        self.currentToDoseRate = float(currentToDoseRate)

    def setAdcToCurrFactor(self, adcToCurrent):
        self.adcToCurrent = float(adcToCurrent)

    def setPedestal(self, pedestal):
        self.pedestal = float(pedestal)

    def appendADCCount(self, doseRate):
        self.adcVec.append(float(doseRate) - self.pedestal)
        self.timeVec.append(float(len(self.timeVec)) / SAMPLING_FREQUENCY)

    def getDoseRate(self):
        doserate = array('d')
        for i in self.adcVec:
            doserate.append(i * self.adcToCurrent * self.currentToDoseRate)
        return doserate

    def getIntegratedDoseInWindow(self):
        integratedDoseVec = array('d')
        doseRateVec = self.getDoseRate()

        for i in range(0, len(doseRateVec) - INTEGRATION_TIME_WINDOW):
            vec = doseRateVec[i:i + INTEGRATION_TIME_WINDOW]
            multVec = [x * TIME_BETWEEN_CONSECUTIVE_DATA for x in vec]
            integratedDoseVec.append(sum(multVec))

        return integratedDoseVec

    def getIntegratedDoseInOneSec(self):
        integratedDoseInOneSec = 0
        doseRateVec = self.getDoseRate()

        for i in range(0, len(doseRateVec)):
            integratedDoseInOneSec = integratedDoseInOneSec + doseRateVec[i] * TIME_BETWEEN_CONSECUTIVE_DATA
        return integratedDoseInOneSec

    def getChannelName(self):
        return self.name

    def plot(self, timeStamp_double, doseRate_double, labelX, labelY):
        gr = TGraph(len(timeStamp_double), timeStamp_double, doseRate_double)
        gr.SetLineColor(2)
        gr.SetLineWidth(4)
        gr.SetMarkerColor(4)
        gr.SetMarkerStyle(21)
        gr.SetMarkerSize(0.35)
        gr.SetTitle(str(self.name))
        gr.GetXaxis().SetLabelOffset(0.02)
        gr.GetXaxis().SetTitleOffset(1.4)
        gr.GetYaxis().SetTitleOffset(1.4)
        gr.GetXaxis().SetTitle(labelX)
        gr.GetYaxis().SetTitle(labelY)
        return gr
