import unittest

from diamond import *


class MyTestCase(unittest.TestCase):
    def test_something(self):
        diamond = Diamond(0)
        diamond.setAdcToCurrFactor(1)
        diamond.setCurrToDoseFactor(1)
        outputVec = []
        for i in range(0, 5000):
            diamond.appendADCCount(1)
            if (i < 4000):
                outputVec.append(1000)
        integratedVec = diamond.getIntegratedDoseInWindow()
        self.assertEqual(outputVec[0], integratedVec[0])
        self.assertEqual(outputVec[5], integratedVec[5])
        self.assertEqual(len(outputVec), len(integratedVec))

    def test_slidingWindows(self):
        diamond = Diamond(0)
        diamond.setAdcToCurrFactor(1)
        diamond.setCurrToDoseFactor(1)

        for i in range(1, 201):
            diamond.appendADCCount(i)
            # first element is 1 last is 200

        integratedVec = diamond.getIntegratedDoseInWindow()
        self.assertEqual(len(integratedVec), 100)
        self.assertAlmostEqual(integratedVec[0], (100 * (100 + 1) / 2) * 0.00001)
        self.assertAlmostEqual(integratedVec[1], ((100 * (100 + 1) / 2) + 100) * 0.00001)
        self.assertAlmostEqual(integratedVec[2], ((100 * (100 + 1) / 2) + 200) * 0.00001)

        self.assertAlmostEqual(integratedVec[7], ((100 * (100 + 1) / 2) + 700) * 0.00001)
        self.assertAlmostEqual(integratedVec[99], ((100 * (100 + 1) / 2) + 9900) * 0.00001)


if __name__ == '__main__':
    unittest.main()
