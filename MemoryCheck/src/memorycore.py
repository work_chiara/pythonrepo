import os
import re
import sys
import xmltodict
from ROOT import TCanvas, TGraph

from diamond import *


class MemoryCore:
    diamondList = []
    iptodcu = {"192.168.13.13": "DCU1", "192.168.13.14": "DCU2",
               "192.168.13.15": "DCU3", "192.168.13.16": "DCU4",
               "192.168.13.17": "DCU5", "192.168.13.18": "DCU6"}

    def readFile(self, dataFile):
        print dataFile
        infile = open(dataFile, "r")
        channel = -1
        for line in infile:
            if (line.startswith("Reading Chanel")):
                parts = re.compile(",\s|\s").split(line)
                channel = int(parts[2])
                self.diamondList.insert(channel, Diamond(channel))
            elif (line.startswith("Conv:DR")):
                parts = re.compile("=|\s").split(line)
                currentToDoseRate = parts[1]
                self.diamondList[channel].setCurrToDoseFactor(currentToDoseRate)
            elif (line.startswith("Conv:Curr")):
                parts = re.compile("=|\s|,\s").split(line)
                adcToCurrent = parts[1]
                pedestal = parts[3]
                self.diamondList[channel].setAdcToCurrFactor(adcToCurrent)
                self.diamondList[channel].setPedestal(pedestal)
            else:
                parts = re.compile(";\s|\s").split(line)
                adcValue = parts[1]
                self.diamondList[channel].appendADCCount(adcValue)

    def getDiamond(self, channel):
        return self.diamondList[channel]

    def plotAdcVsTime(self):
        for channel in self.diamondList:
            c9 = TCanvas('c9', "ADC values vs time. 1s history", 900, 600)
            plot = channel.plot(channel.timeVec, channel.adcVec, "Time [s]", "ADC Counts")
            plot.Draw()
            plot.SaveAs("adc_value" + str(channel.name) + ".root")
        return 0

    def plotDoserateVsTime(self):
        for channel in self.diamondList:
            c9 = TCanvas('c9', "DoseRate values vs time. 1s history", 900, 600)
            dataVec = channel.getDoseRate()
            plot = channel.plot(channel.timeVec, dataVec, "Time [s]", "Doserate [mrad/s]")
            plot.Draw()
            plot.SaveAs("DoseRate_value" + str(channel.name) + ".root")
        return 0

    def plotInRunningWindow(self):
        for channel in self.diamondList:
            c9 = TCanvas('c9', "Integrated dose in running window. 1s history", 900, 600)
            dataVec = channel.getIntegratedDoseInWindow()
            plot = channel.plot(channel.timeVec, dataVec, "Interval of time", "Integrated dose [mrad]")
            plot.Draw()
            plot.SaveAs("IntegratedInRunningWindow_value" + str(channel.name) + ".root")
        return 0

    def IntegratedDoseInOneSec(self):
        for channel in self.diamondList:
            print channel.getIntegratedDoseInOneSec()
        return 0

    def parse(self, xmlFile, ip):
        nameDcu = self.iptodcu[ip]
        f = open(xmlFile, "r")
        xml = f.read()
        confDict = xmltodict.parse(xml)
        env_ = confDict["Env"]
        dcuList = env_["DCUs"]
        dcu = dcuList[nameDcu]
        rangeUsed = dcu["@Range"]
        chan = "Chan%s"
        adcConv = "@C%s"
        for i in range(0, 4):
            diamond = self.getDiamond(i)
            channel = dcu[chan % (i)]
            pedestal = channel["@Ped"]
            currentToDoseRate = channel["@F"]
            adcToCurrent = channel[adcConv % (rangeUsed)]

            diamond.setPedestal(pedestal)
            diamond.setAdcToCurrFactor(adcToCurrent)
            diamond.setCurrToDoseFactor(currentToDoseRate)


def main():
    dataFile = sys.argv[1]

    memoryCore = MemoryCore()
    memoryCore.readFile(dataFile)

    if len(sys.argv) > 2:
        xmlFile = sys.argv[2]
        filename = os.path.basename(dataFile)
        ipSearch = re.search('(?:[0-9]{1,3}\.){3}[0-9]{1,3}', filename)
        if ipSearch == None:
            print 'File name with no IP. Aborting!'
            return

        ip = ipSearch.group(0)
        memoryCore.parse(xmlFile, ip)

    memoryCore.plotAdcVsTime()
    memoryCore.plotDoserateVsTime()
    memoryCore.plotInRunningWindow()
    memoryCore.IntegratedDoseInOneSec()


if __name__ == "__main__":
    main()
