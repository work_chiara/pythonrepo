from unittest import TestCase

from integrateDose import integrateDose


class TestIntegrateDose(TestCase):

    def test_readSingleFile(self):
        iDose = integrateDose()

        fileList = ['sources/test_doserate.txt']
        for file in fileList:
            iDose.readFile(file)

        self.assertEqual(iDose.dia_FWD0.getIntegratedDose(), 3661)
        self.assertEqual(iDose.dia_FWD1.getIntegratedDose(), 3662)
        self.assertEqual(iDose.dia_FWD2.getIntegratedDose(), 3663)
        self.assertEqual(iDose.dia_FWD3.getIntegratedDose(), 3664)


    def test_readTwoFile(self):
        iDose = integrateDose()

        fileList = ['sources/test_doserate.txt','sources/test_doserate2.txt']
        for file in fileList:
            iDose.readFile(file)

        self.assertEqual(iDose.dia_FWD0.getIntegratedDose(), 3661*2)
        self.assertEqual(iDose.dia_FWD1.getIntegratedDose(), 3662*2)
        self.assertEqual(iDose.dia_FWD2.getIntegratedDose(), 3663*2)
        self.assertEqual(iDose.dia_FWD3.getIntegratedDose(), 3664*2)
