from array import array

from datetime import datetime

from ROOT import TGraph, TH1F

DATE_20180419_00_30_00 = '2018-04-19 00:30:00.00'
DATE_20180529_00_00_00 = '2018-05-29 00:00:00.00'


class Diamond:
    def __init__(self, name, thr, ped):
        self.name = name
        self.threshold = thr
        self.pedestal = ped
        self.doseRate_vec, self.timeStamp_vec = array('d'), list()
        self.partialIntDose_vec = array('d')

    def add(self, doseRate, timeStamp):
        #if timeStamp < datetime.strptime('2018-04-19 00:28:00', "%Y-%m-%d %H:%M:%S"):
        #    self.doseRate_vec.append(doseRate - self.pedestal['2018-04-19 00:28:00'])
        #else:
        self.doseRate_vec.append(doseRate)
        self.timeStamp_vec.append(timeStamp)

    def getIntegratedDose(self):
        integratedDose = 0
        self.partialTime_vec = list()
        self.partialIntDose_vec.append(integratedDose)
        for i in range(len(self.doseRate_vec) - 1):
            thres=0
            if self.timeStamp_vec[i] < datetime.strptime('2018-04-09 14:40:00.00', "%Y-%m-%d %H:%M:%S.%f"):
                thres = self.threshold['2018-03-24 21:03:00']
            elif (self.timeStamp_vec[i] < datetime.strptime('2018-04-11 00:04:00.00', "%Y-%m-%d %H:%M:%S.%f") and
                   self.timeStamp_vec[i] > datetime.strptime('2018-04-09 14:40:00.00', "%Y-%m-%d %H:%M:%S.%f")):
                thres = self.threshold['2018-04-09 14:40:00']
            elif (self.timeStamp_vec[i] < datetime.strptime('2018-04-11 15:55:00.00', "%Y-%m-%d %H:%M:%S.%f") and
                   self.timeStamp_vec[i] > datetime.strptime('2018-04-11 00:04:00.00', "%Y-%m-%d %H:%M:%S.%f")):
                thres = self.threshold['2018-04-11 00:04:00']
            elif (self.timeStamp_vec[i] > datetime.strptime('2018-04-11 15:55:00.00', "%Y-%m-%d %H:%M:%S.%f") and
                  self.timeStamp_vec[i] < datetime.strptime(DATE_20180419_00_30_00, "%Y-%m-%d %H:%M:%S.%f")):
                thres = self.threshold['2018-04-11 15:55:00']
            elif (self.timeStamp_vec[i] > datetime.strptime(DATE_20180419_00_30_00, "%Y-%m-%d %H:%M:%S.%f") and
            self.timeStamp_vec[i]< datetime.strptime(DATE_20180529_00_00_00, "%Y-%m-%d %H:%M:%S.%f")):
                thres = self.threshold['2018-04-19 00:30:00']
            elif (self.timeStamp_vec[i] > datetime.strptime(DATE_20180529_00_00_00, "%Y-%m-%d %H:%M:%S.%f")):
                thres = self.threshold['2018-05-29 00:00:00']
            if self.doseRate_vec[i] > thres:
                diff = self.timeStamp_vec[i + 1] - self.timeStamp_vec[i]
                integratedDose = integratedDose + self.doseRate_vec[i] * diff.total_seconds()
            self.partialIntDose_vec.append(integratedDose)
        return integratedDose

    def getIntegratedDoseNoThres(self):
        integratedDose = 0
        self.partialTime_vec = list()
        for i in range(len(self.doseRate_vec) - 1):
            diff = self.timeStamp_vec[i + 1] - self.timeStamp_vec[i]
            integratedDose = integratedDose + self.doseRate_vec[i] * diff.total_seconds()
        return integratedDose

    def plotDoseRate(self):
        gr = self.plot(self.doseRate_vec)
        return gr

    def plotIntDose(self):
        gr = self.plot(self.partialIntDose_vec)
        return gr

    def plot(self, vec_doseRate):
        timeStamp_double = array('d')
        for i in self.timeStamp_vec:
            timeStamp_double.append(float(i.strftime("%s")))
        gr = TGraph(len(timeStamp_double), timeStamp_double, vec_doseRate)
        gr.SetLineColor(2)
        gr.SetLineWidth(4)
        gr.SetMarkerColor(4)
        gr.SetMarkerStyle(21)
        gr.SetMarkerSize(0.35)
        gr.SetTitle('')
        gr.GetXaxis().SetTimeDisplay(1)
        gr.GetXaxis().SetNdivisions(5, 5, 0, 0)
        #gr.GetXaxis().SetTimeFormat("#splitline{%m-%d}{h%H}%F1970-01-01 00:00:00")
        gr.GetXaxis().SetTimeFormat("#splitline{}{%H:%M:%S}%F1970-01-01 00:00:00")
        gr.GetXaxis().SetLabelOffset(0.02)
        gr.GetXaxis().SetTitleOffset(1.4)
        gr.GetYaxis().SetTitleOffset(1.4)
        gr.GetXaxis().SetTitle("date [h-m]")
        gr.GetYaxis().SetTitle('dose rate [mRad/s]')
        return gr


    def getName(self):
        return self.name

    def plotHisto(self, initDate, endDate):
        histo = TH1F("histo", "histo", 100, -1, 1)
        startTime = datetime.strptime(endDate, "%Y-%m-%d %H:%M:%S")
        endTime = datetime.strptime(initDate, "%Y-%m-%d %H:%M:%S")
        for i in range(len(self.doseRate_vec) - 1):
            if self.timeStamp_vec[i] < startTime and self.timeStamp_vec[i] > endTime:
                histo.Fill(self.doseRate_vec[i])
        return histo

    def plotHistoOneHz(self, initDate, endDate):
        histo = TH1F("histo", "histo", 100, -3, 5)
        startTime = datetime.strptime(endDate, "%Y-%m-%d %H:%M:%S")
        endTime = datetime.strptime(initDate, "%Y-%m-%d %H:%M:%S")
        doseMean=0
        for i in range(len(self.doseRate_vec) - 1):
            if self.timeStamp_vec[i] < startTime and self.timeStamp_vec[i] > endTime:
                doseMean = doseMean + self.doseRate_vec[i]
                if i%10 == 0:
                    doseMean = doseMean/10
                    histo.Fill(doseMean)
                    doseMean=0
        return histo

    def returnVecDoseRate(self):
        return self.doseRate_vec

    def returnVecTimestamp(self, startPeriod, endPeriod):
        timeStamp_vec_timeRange = list()
        for i in range(len(self.timeStamp_vec)):
            if (self.timeStamp_vec[i] > datetime.strptime(startPeriod, "%Y-%m-%d %H:%M:%S.%f") and
                    self.timeStamp_vec[i] < datetime.strptime(endPeriod, "%Y-%m-%d %H:%M:%S.%f")):
                timeStamp_vec_timeRange.append(self.timeStamp_vec[i])
            if (self.timeStamp_vec[i] > datetime.strptime(endPeriod, "%Y-%m-%d %H:%M:%S.%f")):
                break
        return timeStamp_vec_timeRange
