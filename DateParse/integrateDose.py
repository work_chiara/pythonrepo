#!/usr/bin/python

import sys
import time
from array import array
from datetime import datetime

from Diamond import Diamond
from ROOT import TCanvas, TLegend, TGraph


class integrateDose:

    def __init__(self):
        # threshold of 2018-05-29 00:00:00 from data of 31/05 from 9:30 to 15:00
        self.dia_FWD0 = Diamond("FWD0", {'2018-03-24 21:03:00': 0.0028, '2018-04-09 14:40:00': 0.3,
                                         '2018-04-11 00:04:00': 0.0028, '2018-04-11 15:55:00': 0.3,
                                         '2018-04-19 00:30:00': 0.0028, '2018-05-29 00:00:00': 0.3},
                                {'2018-03-24 21:03:00': 0, '2018-04-19 00:28:00': 0})
        self.dia_FWD1 = Diamond("FWD1", {'2018-03-24 21:03:00': 0.0021, '2018-04-09 14:40:00': 0.26,
                                         '2018-04-11 00:04:00': 0.0021, '2018-04-11 15:55:00': 0.26,
                                         '2018-04-19 00:30:00': 0.0021, '2018-05-29 00:00:00': 0.3},
                                {'2018-03-24 21:03:00': 0, '2018-04-19 00:28:00': 0})
        self.dia_FWD2 = Diamond("FWD2", {'2018-03-24 21:03:00': 0.0037, '2018-04-09 14:40:00': 0.44,
                                         '2018-04-11 00:04:00': 0.0037, '2018-04-11 15:55:00': 0.44,
                                         '2018-04-19 00:30:00': 0.0037, '2018-05-29 00:00:00': 0.5},
                                {'2018-03-24 21:03:00': 0, '2018-04-19 00:28:00': 0})
        self.dia_FWD3 = Diamond("FWD3", {'2018-03-24 21:03:00': 0.0033, '2018-04-09 14:40:00': 0.23,
                                         '2018-04-11 00:04:00': 0.0033, '2018-04-11 15:55:00': 0.23,
                                         '2018-04-19 00:30:00': 0.0033, '2018-05-29 00:00:00': 0.3},
                                {'2018-03-24 21:03:00': 0, '2018-04-19 00:28:00': 0})
        self.dia_BWD0 = Diamond("BWD0", {'2018-03-24 21:03:00': 0, '2018-04-09 14:40:00': 0,
                                         '2018-04-11 00:04:00': 0, '2018-04-11 15:55:00': 0,
                                         '2018-04-19 00:30:00': 0.008, '2018-05-29 00:00:00': 0.008},
                                {'2018-03-24 21:03:00': 0, '2018-04-19 00:28:00': 0,
                                 '2018-06-09 00:00:00': 0})
        self.dia_BWD1 = Diamond("BWD1", {'2018-03-24 21:03:00': 0, '2018-04-09 14:40:00': 0,
                                         '2018-04-11 00:04:00': 0, '2018-04-11 15:55:00': 0,
                                         '2018-04-19 00:30:00': 0.007, '2018-05-29 00:00:00': 0.007},
                                {'2018-03-24 21:03:00': 0, '2018-04-19 00:28:00': 0,
                                 '2018-06-09 00:00:00': 0})
        self.dia_BWD2 = Diamond("BWD2", {'2018-03-24 21:03:00': 0, '2018-04-09 14:40:00': 0,
                                         '2018-04-11 00:04:00': 0, '2018-04-11 15:55:00': 0,
                                         '2018-04-19 00:30:00': 0.009, '2018-05-29 00:00:00': 0.009},
                                {'2018-03-24 21:03:00': 0, '2018-04-19 00:28:00': 0,
                                 '2018-06-09 00:00:00': 0})
        self.dia_BWD3 = Diamond("BWD3", {'2018-03-24 21:03:00': 0, '2018-04-09 14:40:00': 0,
                                         '2018-04-11 00:04:00': 0, '2018-04-11 15:55:00': 0,
                                         '2018-04-19 00:30:00': 0.006, '2018-05-29 00:00:00': 0.006},
                                {'2018-03-24 21:03:00': 0, '2018-04-19 00:28:00': 0,
                                 '2018-06-09 00:00:00': 0})

    def makePlots(self):
        c1 = TCanvas('c1', 'FWD0', 900, 600)
        gr = self.dia_FWD0.plotDoseRate()
        gr.Draw('AP')
        c1.SaveAs("doseRate_FWD0.root")

        c2 = TCanvas('c2', 'FWD1', 900, 600)
        gr_2 = self.dia_FWD1.plotDoseRate()
        gr_2.Draw('AP')
        c2.SaveAs("doseRate_FWD1.root")

        c3 = TCanvas('c3', 'FWD2', 900, 600)
        gr_3 = self.dia_FWD2.plotDoseRate()
        gr_3.Draw('AP')
        c3.SaveAs("doseRate_FWD2.root")

        c4 = TCanvas('c4', 'FWD3', 900, 600)
        gr_4 = self.dia_FWD3.plotDoseRate()
        gr_4.Draw('AP')
        c4.SaveAs("doseRate_FWD3.root")

        c_all = TCanvas('c_all', 'all FWD', 900, 600)
        gr_3.SetMarkerColor(4)
        gr.SetMarkerColor(3)
        gr_2.SetMarkerColor(2)
        gr_4.SetMarkerColor(1)
        gr_3.Draw('AP')
        gr_4.Draw('PSAME')
        gr_2.Draw('PSAME')
        gr.Draw('PSAME')
        legend = TLegend(0.1, 0.7, 0.48, 0.9)
        legend.AddEntry(gr, "FW_phi55", "p")
        legend.AddEntry(gr_2, "FW_phi125", "p")
        legend.AddEntry(gr_3, "FW_phi235", "p")
        legend.AddEntry(gr_4, "FW_phi305", "p")
        legend.Draw()
        c_all.SaveAs("allFWD.root")

        c5 = TCanvas('c5', 'BWD0', 900, 600)
        gr_5 = self.dia_BWD0.plotDoseRate()
        gr_5.Draw('AP')
        c5.SaveAs("doseRate_BWD0.root")

        c6 = TCanvas('c6', 'BWD1', 900, 600)
        gr_6 = self.dia_BWD1.plotDoseRate()
        gr_6.Draw('AP')
        c6.SaveAs("doseRate_BWD1.root")

        c7 = TCanvas('c7', 'BWD2', 900, 600)
        gr_7 = self.dia_BWD2.plotDoseRate()
        gr_7.Draw('AP')
        c7.SaveAs("doseRate_BWD2.root")

        c8 = TCanvas('c8', 'BWD3', 900, 600)
        gr_8 = self.dia_BWD3.plotDoseRate()
        gr_8.Draw('AP')
        c8.SaveAs("doseRate_BWD3.root")

        c_all_bw = TCanvas('c_all_bw', 'all BWD', 900, 600)
        gr_5.SetMarkerColor(4)
        gr_6.SetMarkerColor(3)
        gr_7.SetMarkerColor(2)
        gr_8.SetMarkerColor(1)
        gr_8.Draw('AP')
        gr_5.Draw('PSAME')
        gr_6.Draw('PSAME')
        gr_7.Draw('PSAME')
        legend = TLegend(0.1, 0.7, 0.48, 0.9)
        legend.AddEntry(gr_5, "BW_phi55", "p")
        legend.AddEntry(gr_6, "BW_phi125", "p")
        legend.AddEntry(gr_7, "BW_phi235", "p")
        legend.AddEntry(gr_8, "BW_phi305", "p")
        legend.Draw()
        c_all_bw.SaveAs("allBWD.root")

        c_all_dia = TCanvas('c_all_dia', 'all diamonds', 900, 600)
        gr_5.SetMarkerColor(4)
        gr_6.SetMarkerColor(3)
        gr_7.SetMarkerColor(2)
        gr_8.SetMarkerColor(1)
        gr.SetMarkerColor(5)
        gr_2.SetMarkerColor(6)
        gr_3.SetMarkerColor(7)
        gr_4.SetMarkerColor(9)
        gr_4.Draw('AP')
        gr.Draw('PSAME')
        gr_2.Draw('PSAME')
        gr_3.Draw('PSAME')
        gr_8.Draw('PSAME')
        gr_5.Draw('PSAME')
        gr_6.Draw('PSAME')
        gr_7.Draw('PSAME')
        legend = TLegend(0.1, 0.7, 0.48, 0.9)
        legend.AddEntry(gr_5, "BW_phi55", "p")
        legend.AddEntry(gr_6, "BW_phi125", "p")
        legend.AddEntry(gr_7, "BW_phi235", "p")
        legend.AddEntry(gr_8, "BW_phi305", "p")
        legend.AddEntry(gr, "FW_phi55", "p")
        legend.AddEntry(gr_2, "FW_phi125", "p")
        legend.AddEntry(gr_3, "FW_phi235", "p")
        legend.AddEntry(gr_4, "FW_phi305", "p")
        legend.Draw()
        c_all_dia.SaveAs("allDIAMONDS.root")

        c9 = TCanvas('c9', 'integrated dose BWD0', 900, 600)
        gr_9 = self.dia_BWD0.plotIntDose()
        gr_9.Draw()
        c9.SaveAs("integrated_BWD0.root")

        c10 = TCanvas('c10', 'integrated dose BWD1', 900, 600)
        gr_10 = self.dia_BWD1.plotIntDose()
        gr_10.Draw()
        c10.SaveAs("integrated_BWD1.root")

        c11 = TCanvas('c11', 'integrated dose BWD2', 900, 600)
        gr_11 = self.dia_BWD2.plotIntDose()
        gr_11.Draw()
        c11.SaveAs("integrated_BWD2.root")

        c12 = TCanvas('c12', 'integrated dose BWD3', 900, 600)
        gr_12 = self.dia_BWD3.plotIntDose()
        gr_12.Draw()
        c12.SaveAs("integrated_BWD3.root")

        # self.plotHisto('2018-04-30 22:10:00', '2018-04-30 22:43:00', 'her_40')
        # self.plotHisto('2018-04-30 23:05:00', '2018-04-30 23:38:00', 'her_80')
        # self.plotHisto('2018-05-01 00:11:00', '2018-05-01 00:20:00', 'her_120')
        # self.plotHisto('2018-05-01 00:44:00', '2018-05-01 01:06:00', 'her_160')
        # self.plotHisto('2018-05-01 03:40:00', '2018-05-01 03:48:00', 'ler_40')
        # self.plotHisto('2018-05-01 04:16:00', '2018-05-01 04:32:00', 'ler_80')
        # self.plotHisto('2018-05-01 05:25:00', '2018-05-01 05:41:00', 'ler_120')
        # self.plotHisto('2018-05-01 06:13:00', '2018-05-01 06:29:00', 'ler_160')

    #        self.plotHistoOneHz('2018-04-15 15:00:00', '2018-04-15 21:00:00')

    def plotHisto(self, start, end, id):
        c13 = TCanvas('c13', 'BWD0 histo', 900, 600)
        h_0 = self.dia_BWD0.plotHisto(start, end)
        h_0.Draw()
        c13.SaveAs("histo_BWD0_" + id + ".root")
        c14 = TCanvas('c14', 'BWD1 histo', 900, 600)
        h_1 = self.dia_BWD1.plotHisto(start, end)
        h_1.Draw()
        c14.SaveAs("histo_BWD1_" + id + ".root")
        c15 = TCanvas('c15', 'BWD2 histo', 900, 600)
        h_2 = self.dia_BWD2.plotHisto(start, end)
        h_2.Draw()
        c15.SaveAs("histo_BWD2_" + id + ".root")
        c16 = TCanvas('c16', 'BWD3 histo', 900, 600)
        h_3 = self.dia_BWD3.plotHisto(start, end)
        h_3.Draw()
        c16.SaveAs("histo_BWD3_" + id + ".root")

        c13_f = TCanvas('c13_f', 'FWD0 histo', 900, 600)
        h_0_f = self.dia_FWD0.plotHisto(start, end)
        h_0_f.Draw()
        c13_f.SaveAs("histo_FWD0_" + id + ".root")
        c14_f = TCanvas('c14_f', 'FWD1 histo', 900, 600)
        h_1_f = self.dia_FWD1.plotHisto(start, end)
        h_1_f.Draw()
        c14_f.SaveAs("histo_FWD1_" + id + ".root")
        c15_f = TCanvas('c15_f', 'FWD2 histo', 900, 600)
        h_2_f = self.dia_FWD2.plotHisto(start, end)
        h_2_f.Draw()
        c15_f.SaveAs("histo_FWD2_" + id + ".root")
        c16_f = TCanvas('c16_f', 'FWD3 histo', 900, 600)
        h_3_f = self.dia_FWD3.plotHisto(start, end)
        h_3_f.Draw()
        c16_f.SaveAs("histo_FWD3_" + id + ".root")

        # cScatter = TCanvas('cScatter', 'scatter plot fwd0 vs bwd0', 900, 600)
        # gr=self.scatterPlot("2018-05-10 18:21:18.897222","2018-05-10 18:21:19.00",self.dia_FWD0, self.dia_BWD0)
        # gr.Draw()
        # cScatter.SaveAs("scatter_FWD0_vs_BWD0.png")

    def scatterPlot(self, startPeriod, endPeriod, dia1, dia2):

        doseRateVec_dia1 = dia1.returnVecDoseRate()
        timeVec_dia1 = dia1.returnVecTimestamp(startPeriod, endPeriod)

        doseRateVec_dia2 = dia2.returnVecDoseRate()
        timeVec_dia2 = dia2.returnVecTimestamp(startPeriod, endPeriod)

        doseRateVecTimeSync_dia1 = array('d')
        doseRateVecTimeSync_dia2 = array('d')

        for i in range(len(timeVec_dia1)):
            for j in range(len(timeVec_dia2)):
                if (abs(self.getUnixTime(timeVec_dia1[i]) - self.getUnixTime(timeVec_dia2[j]))) < 0.1:
                    doseRateVecTimeSync_dia1.append(doseRateVec_dia1[i])
                    doseRateVecTimeSync_dia2.append(doseRateVec_dia2[j])
                    continue

        gr = TGraph(doseRateVecTimeSync_dia2, doseRateVecTimeSync_dia1)
        gr.SetLineColor(2)
        gr.SetLineWidth(4)
        gr.SetMarkerColor(4)
        gr.SetMarkerStyle(21)
        gr.SetMarkerSize(0.35)
        gr.SetTitle('')
        gr.GetXaxis().SetTitle("dose rate [mRad/s]")
        gr.GetYaxis().SetTitle('dose rate [mRad/s]')
        return gr

    # def plotHistoOneHz(self, start, end):
    #     c13 = TCanvas('c13', 'FWD0 histo', 900, 600)
    #     h_0 = self.dia_FWD0.plotHistoOneHz(start, end)
    #     h_0.Draw()
    #     c13.SaveAs("histo1Hz_FWD0.root")
    #     c14 = TCanvas('c14', 'FWD1 histo', 900, 600)
    #     h_1 = self.dia_FWD1.plotHistoOneHz(start, end)
    #     h_1.Draw()
    #     c14.SaveAs("histo1Hz_FWD1.root")
    #     c15 = TCanvas('c15', 'FWD2 histo', 900, 600)
    #     h_2 = self.dia_FWD2.plotHistoOneHz(start, end)
    #     h_2.Draw()
    #     c15.SaveAs("histo1Hz_FWD2.root")
    #     c16 = TCanvas('c16', 'FWD3 histo', 900, 600)
    #     h_3 = self.dia_FWD3.plotHistoOneHz(start, end)
    #     h_3.Draw()
    #     c16.SaveAs("histo1Hz_FWD3.root")

    def calculateIntegrateDose(self):
        intDose_FWD0 = self.dia_FWD0.getIntegratedDose()
        intDose_FWD1 = self.dia_FWD1.getIntegratedDose()
        intDose_FWD2 = self.dia_FWD2.getIntegratedDose()
        intDose_FWD3 = self.dia_FWD3.getIntegratedDose()
        intDose_BWD0 = self.dia_BWD0.getIntegratedDose()
        intDose_BWD1 = self.dia_BWD1.getIntegratedDose()
        intDose_BWD2 = self.dia_BWD2.getIntegratedDose()
        intDose_BWD3 = self.dia_BWD3.getIntegratedDose()

        print "integrated Dose FWD0 = " + str(intDose_FWD0)
        print "integrated Dose FWD1 = " + str(intDose_FWD1)
        print "integrated Dose FWD2 = " + str(intDose_FWD2)
        print "integrated Dose FWD3 = " + str(intDose_FWD3)
        print "integrated Dose BWD0 = " + str(intDose_BWD0)
        print "integrated Dose BWD1 = " + str(intDose_BWD1)
        print "integrated Dose BWD2 = " + str(intDose_BWD2)
        print "integrated Dose BWD3 = " + str(intDose_BWD3)

    def calculateIntegrateDoseNoThres(self):
        intDose_FWD0 = self.dia_FWD0.getIntegratedDoseNoThres()
        intDose_FWD1 = self.dia_FWD1.getIntegratedDoseNoThres()
        intDose_FWD2 = self.dia_FWD2.getIntegratedDoseNoThres()
        intDose_FWD3 = self.dia_FWD3.getIntegratedDoseNoThres()
        intDose_BWD0 = self.dia_BWD0.getIntegratedDoseNoThres()
        intDose_BWD1 = self.dia_BWD1.getIntegratedDoseNoThres()
        intDose_BWD2 = self.dia_BWD2.getIntegratedDoseNoThres()
        intDose_BWD3 = self.dia_BWD3.getIntegratedDoseNoThres()

        print str(intDose_FWD0) + "\t" + str(intDose_FWD1) + "\t" + str(intDose_FWD2) + "\t" + str(
            intDose_FWD3) + "\t" + \
              str(intDose_BWD0) + "\t" + str(intDose_BWD1) + "\t" + str(intDose_BWD2) + "\t" + str(intDose_BWD3)

    def readFile(self, filename):
        print filename
        infile = open(filename, "r")
        for line in infile:
            parts = line.split()
            data = parts[1]
            hour = parts[2]
            doseRateString = parts[3]
            if doseRateString == "***":
                continue
            doseRate = 0
            try:
                doseRate = float(doseRateString)
                space = " "
                dataTime = data + space + hour
                timeStamp = datetime.strptime(dataTime, "%Y-%m-%d %H:%M:%S.%f")
            except:
                print "Error on format:" + line
                raise
            if "FWD:Ch0" in line:
                self.dia_FWD0.add(doseRate, timeStamp)
            elif "FWD:Ch1" in line:
                self.dia_FWD1.add(doseRate, timeStamp)
            elif "FWD:Ch2" in line:
                self.dia_FWD2.add(doseRate, timeStamp)
            elif "FWD:Ch3" in line:
                self.dia_FWD3.add(doseRate, timeStamp)
            elif "BWD:Ch0" in line:
                self.dia_BWD0.add(doseRate, timeStamp)
            elif "BWD:Ch1" in line:
                self.dia_BWD1.add(doseRate, timeStamp)
            elif "BWD:Ch2" in line:
                self.dia_BWD2.add(doseRate, timeStamp)
            elif "BWD:Ch3" in line:
                self.dia_BWD3.add(doseRate, timeStamp)
        infile.close()

    def getUnixTime(self, timeStamp_local):
        unixtime = float(time.mktime(timeStamp_local.timetuple()))
        milliseconds = float(timeStamp_local.microsecond) / 1000000
        unixtime_milliseconds = unixtime + milliseconds
        return unixtime_milliseconds


def main():
    iDose = integrateDose()

    fileList = [sys.argv[1]]
        # '/Users/chiaralalicata/LAVORO/BEAST-phaseII/pythonrepo/Monitoring/BEAST_II-diamondDoseRate-2018-05-29.txt']
       # '/Users/chiaralalicata/HD/file_BEAST_phase2/10_06_all/BEAST_II-diamondDoseRate-2018-06-10.txt']
    # '/Users/chiaralalicata/LAVORO/BEAST-phaseII/pythonrepo/Monitoring/fileTest_short.txt']
    # '/Users/chiaralalicata/HD/file_BEAST_phase2/step_CurrentStudy.txt']
    for file in fileList:
        iDose.readFile(file)

    # iDose.calculateIntegrateDose()
    iDose.calculateIntegrateDoseNoThres()
    # iDose.makePlots()


if __name__ == "__main__":
    main()
