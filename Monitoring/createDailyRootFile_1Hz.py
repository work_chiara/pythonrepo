import sys
import re
import time
from array import array
from datetime import datetime

from pytz import timezone

from ROOT import TFile, TNtuple, TTree, gROOT, AddressOf

FILE_TIME_ZONE = 'Asia/Tokyo'
LOCAL_TIME_ZONE = 'Europe/Rome'  # SET THE TIMEZONE OF THE MACHINE RUNNING THIS SCRIPT

gROOT.ProcessLine(
    "struct MyStruct {\
        Double_t faverageDoseRate[8];\
    };");

import ROOT as r


class CreateDailyRootFile:
    def __init__(self, source):
        self.fwd0_nextDoseRate = 0
        self.fwd1_nextDoseRate = 0
        self.fwd2_nextDoseRate = 0
        self.fwd3_nextDoseRate = 0
        self.bwd0_nextDoseRate = 0
        self.bwd1_nextDoseRate = 0
        self.bwd2_nextDoseRate = 0
        self.bwd3_nextDoseRate = 0

        self.numberOfSamples_fwd0 = 0
        self.numberOfSamples_fwd1 = 0
        self.numberOfSamples_fwd2 = 0
        self.numberOfSamples_fwd3 = 0
        self.numberOfSamples_bwd0 = 0
        self.numberOfSamples_bwd1 = 0
        self.numberOfSamples_bwd2 = 0
        self.numberOfSamples_bwd3 = 0

        self.source = source
        self.oldFilename = ""
        self.file = None
        self.t = None
        self.firstTime = 0
        self.ts = array('f', [0.])
        self.averageDoseRate = r.vector('float')(8)
        self.maxDoseRate = r.vector('float')(8)

        self.fwd0_averageDoseRate = 0
        self.fwd1_averageDoseRate = 0
        self.fwd2_averageDoseRate = 0
        self.fwd3_averageDoseRate = 0
        self.bwd0_averageDoseRate = 0
        self.bwd1_averageDoseRate = 0
        self.bwd2_averageDoseRate = 0
        self.bwd3_averageDoseRate = 0
        self.fwd0_maxDoseRate = 0
        self.fwd1_maxDoseRate = 0
        self.fwd2_maxDoseRate = 0
        self.fwd3_maxDoseRate = 0
        self.bwd0_maxDoseRate = 0
        self.bwd1_maxDoseRate = 0
        self.bwd2_maxDoseRate = 0
        self.bwd3_maxDoseRate = 0

        self.finished_fwd0 = False
        self.finished_fwd1 = False
        self.finished_fwd2 = False
        self.finished_fwd3 = False
        self.finished_bwd0 = False
        self.finished_bwd1 = False
        self.finished_bwd2 = False
        self.finished_bwd3 = False

        self.first = 0

        self.missedEntries = 0

    def readStream(self):
        for line in self.source:
            if "WD:Ch" in line:
                parts = line.split()
                newData = parts[1]
                fileName = "BEAST_II-diamondDoseRate-" + newData + ".root"
                if fileName != self.oldFilename:
                    if self.file is not None:
                        print("Closed File " + self.oldFilename)
                        self.file.Write()
                        self.file.Close()
                    self.file = TFile(fileName, "RECREATE")
                    self.oldFilename = fileName
                    self.t = TTree('tout', 'tree')
                    self.t.Branch('ts', self.ts, 'ts/F')
                    self.t.Branch('averageDoseRate', self.averageDoseRate)
                    self.t.Branch('maxDoseRate', self.maxDoseRate)
                    print("Created new File " + fileName)
                diamondName = parts[0]
                data = parts[1]
                hour = parts[2]
                doseRate = float(parts[3])
                space = " "
                dataTime = data + space + hour[0:15]

                unixtime_milliseconds = self.getUnixTime(dataTime)
                unixtime = int(unixtime_milliseconds)

                if self.firstTime == 0:
                    self.firstTime = unixtime

                if "FWD:Ch0" in diamondName:
                    if unixtime - self.firstTime < 1:
                        self.numberOfSamples_fwd0 = self.numberOfSamples_fwd0 + 1
                        self.fwd0_averageDoseRate = self.fwd0_averageDoseRate + doseRate
                        if self.fwd0_maxDoseRate < doseRate:
                            self.fwd0_maxDoseRate = doseRate
                    else:
                        self.finished_fwd0 = True
                        self.fwd0_nextDoseRate = doseRate
                elif "FWD:Ch1" in diamondName:
                    if unixtime - self.firstTime < 1:
                        self.numberOfSamples_fwd1 = self.numberOfSamples_fwd1 + 1
                        self.fwd1_averageDoseRate = self.fwd1_averageDoseRate + doseRate
                        if self.fwd1_maxDoseRate < doseRate:
                            self.fwd1_maxDoseRate = doseRate
                    else:
                        self.finished_fwd1 = True
                        self.fwd1_nextDoseRate = doseRate
                elif "FWD:Ch2" in diamondName:
                    if unixtime - self.firstTime < 1:
                        self.numberOfSamples_fwd2 = self.numberOfSamples_fwd2 + 1
                        self.fwd2_averageDoseRate = self.fwd2_averageDoseRate + doseRate
                        if self.fwd2_maxDoseRate < doseRate:
                            self.fwd2_maxDoseRate = doseRate
                    else:
                        self.finished_fwd2 = True
                        self.fwd2_nextDoseRate = doseRate
                elif "FWD:Ch3" in diamondName:
                    if unixtime - self.firstTime < 1:
                        self.numberOfSamples_fwd3 = self.numberOfSamples_fwd3 + 1
                        self.fwd3_averageDoseRate = self.fwd3_averageDoseRate + doseRate
                        if self.fwd3_maxDoseRate < doseRate:
                            self.fwd3_maxDoseRate = doseRate
                    else:
                        self.finished_fwd3 = True
                        self.fwd3_nextDoseRate = doseRate
                elif "BWD:Ch0" in diamondName:
                    if unixtime - self.firstTime < 1:
                        self.numberOfSamples_bwd0 = self.numberOfSamples_bwd0 + 1
                        self.bwd0_averageDoseRate = self.bwd0_averageDoseRate + doseRate
                        if self.bwd0_maxDoseRate < doseRate:
                            self.bwd0_maxDoseRate = doseRate
                    else:
                        self.finished_bwd0 = True
                        self.bwd0_nextDoseRate = doseRate
                elif "BWD:Ch1" in diamondName:
                    if unixtime - self.firstTime < 1:
                        self.numberOfSamples_bwd1 = self.numberOfSamples_bwd1 + 1
                        self.bwd1_averageDoseRate = self.bwd1_averageDoseRate + doseRate
                        if self.bwd1_maxDoseRate < doseRate:
                            self.bwd1_maxDoseRate = doseRate
                    else:
                        self.finished_bwd1 = True
                        self.bwd1_nextDoseRate = doseRate
                elif "BWD:Ch2" in diamondName:
                    if unixtime - self.firstTime < 1:
                        self.numberOfSamples_bwd2 = self.numberOfSamples_bwd2 + 1
                        self.bwd2_averageDoseRate = self.bwd2_averageDoseRate + doseRate
                        if self.bwd2_maxDoseRate < doseRate:
                            self.bwd2_maxDoseRate = doseRate
                    else:
                        self.finished_bwd2 = True
                        self.bwd2_nextDoseRate = doseRate
                elif "BWD:Ch3" in diamondName:
                    if unixtime - self.firstTime < 1:
                        self.numberOfSamples_bwd3 = self.numberOfSamples_bwd3 + 1
                        self.bwd3_averageDoseRate = self.bwd3_averageDoseRate + doseRate
                        if self.bwd3_maxDoseRate < doseRate:
                            self.bwd3_maxDoseRate = doseRate
                    else:
                        self.finished_bwd3 = True
                        self.bwd3_nextDoseRate = doseRate

                if self.finished_fwd0 and self.finished_fwd1 and self.finished_fwd2 and self.finished_fwd3 and \
                        self.finished_bwd0 and self.finished_bwd1 and self.finished_bwd2 and self.finished_bwd3:

                    self.ts[0] = float(self.firstTime)

                    self.averageDoseRate[0] = float(self.fwd0_averageDoseRate / self.numberOfSamples_fwd0)
                    self.averageDoseRate[1] = float(self.fwd1_averageDoseRate / self.numberOfSamples_fwd1)
                    self.averageDoseRate[2] = float(self.fwd2_averageDoseRate / self.numberOfSamples_fwd2)
                    self.averageDoseRate[3] = float(self.fwd3_averageDoseRate / self.numberOfSamples_fwd3)
                    self.averageDoseRate[4] = float(self.bwd0_averageDoseRate / self.numberOfSamples_bwd0)
                    self.averageDoseRate[5] = float(self.bwd1_averageDoseRate / self.numberOfSamples_bwd1)
                    self.averageDoseRate[6] = float(self.bwd2_averageDoseRate / self.numberOfSamples_bwd2)
                    self.averageDoseRate[7] = float(self.bwd3_averageDoseRate / self.numberOfSamples_bwd3)

                    self.maxDoseRate[0] = float(self.fwd0_maxDoseRate)
                    self.maxDoseRate[1] = float(self.fwd1_maxDoseRate)
                    self.maxDoseRate[2] = float(self.fwd2_maxDoseRate)
                    self.maxDoseRate[3] = float(self.fwd3_maxDoseRate)
                    self.maxDoseRate[4] = float(self.bwd0_maxDoseRate)
                    self.maxDoseRate[5] = float(self.bwd1_maxDoseRate)
                    self.maxDoseRate[6] = float(self.bwd2_maxDoseRate)
                    self.maxDoseRate[7] = float(self.bwd3_maxDoseRate)

                    self.push()
                    self.firstTime = unixtime
                    self.initDoseRate()
        self.file.Write()
        self.file.Close()

    def initDoseRate(self):
        self.finished_fwd0 = False
        self.finished_fwd1 = False
        self.finished_fwd2 = False
        self.finished_fwd3 = False
        self.finished_bwd0 = False
        self.finished_bwd1 = False
        self.finished_bwd2 = False
        self.finished_bwd3 = False

        self.fwd0_averageDoseRate = self.fwd0_nextDoseRate
        self.fwd0_maxDoseRate = self.fwd0_nextDoseRate
        self.numberOfSamples_fwd0 = 1
        self.fwd1_averageDoseRate = self.fwd1_nextDoseRate
        self.fwd1_maxDoseRate = self.fwd1_nextDoseRate
        self.numberOfSamples_fwd1 = 1
        self.fwd2_averageDoseRate = self.fwd2_nextDoseRate
        self.fwd2_maxDoseRate = self.fwd2_nextDoseRate
        self.numberOfSamples_fwd2 = 1
        self.fwd3_averageDoseRate = self.fwd3_nextDoseRate
        self.fwd3_maxDoseRate = self.fwd3_nextDoseRate
        self.numberOfSamples_fwd3 = 1
        self.bwd0_averageDoseRate = self.bwd0_nextDoseRate
        self.bwd0_maxDoseRate = self.bwd0_nextDoseRate
        self.numberOfSamples_bwd0 = 1
        self.bwd1_averageDoseRate = self.bwd1_nextDoseRate
        self.bwd1_maxDoseRate = self.bwd1_nextDoseRate
        self.numberOfSamples_bwd1 = 1
        self.bwd2_averageDoseRate = self.bwd2_nextDoseRate
        self.bwd2_maxDoseRate = self.bwd2_nextDoseRate
        self.numberOfSamples_bwd2 = 1
        self.bwd3_averageDoseRate = self.bwd3_nextDoseRate
        self.bwd3_maxDoseRate = self.bwd3_nextDoseRate
        self.numberOfSamples_bwd3 = 1

    def push(self):
        self.t.Fill()

    def getUnixTime(self, dataTime):
        timeStamp_naive = datetime.strptime(dataTime, "%Y-%m-%d %H:%M:%S.%f")
        timeStamp_jst = timeStamp_naive.replace(tzinfo=timezone(FILE_TIME_ZONE))
        timeStamp_local = timeStamp_jst.astimezone(timezone(LOCAL_TIME_ZONE))
        unixtime = float(time.mktime(timeStamp_local.timetuple()))
        milliseconds = float(timeStamp_local.microsecond) / 1000000
        unixtime_milliseconds = unixtime + milliseconds
        # print("JST: " + str(timeStamp_jst))
        # print("Rome: " + str(timeStamp_local))
        # print("Unix Time: " + str(unixtime_milliseconds))
        return unixtime_milliseconds


def main():
    source = sys.stdin
    if len(sys.argv) > 1:
        source = open(sys.argv[1], "r")
    createFile = CreateDailyRootFile(source)
    createFile.readStream()


if __name__ == "__main__":
    main()
