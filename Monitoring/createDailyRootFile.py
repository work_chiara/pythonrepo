import sys
import re
import time
from datetime import datetime

from pytz import timezone

from ROOT import TFile, TNtuple, TTree, gROOT

FILE_TIME_ZONE = 'Asia/Tokyo'
LOCAL_TIME_ZONE = 'Europe/Rome'  # SET THE TIMEZONE OF THE MACHINE RUNNING THIS SCRIPT

gROOT.ProcessLine(
    "struct MyStruct {\
        Double_t   timestamp;\
        Double_t     doseRate;\
    };");

from ROOT import MyStruct


class CreateDailyRootFile:
    def __init__(self, source):
        self.source = source
        self.oldFilename = ""
        self.file = None
        self.t = None
        self.FWD_0 = MyStruct()
        self.FWD_1 = MyStruct()
        self.FWD_2 = MyStruct()
        self.FWD_3 = MyStruct()
        self.fwd0_enter = False
        self.fwd1_enter = False
        self.fwd2_enter = False
        self.fwd3_enter = False
        self.missedEntries = 0

    def readStream(self):
        fwd0Entries = 0
        for line in self.source:
            if "WD:Ch" in line:
                parts = line.split()
                newData = parts[1]
                fileName = "BEAST_II-diamondDoseRate-" + newData + ".root"
                if fileName != self.oldFilename:
                    if self.file is not None:
                        print("Closed File " + self.oldFilename)
                        self.file.Write()
                        self.file.Close()
                    self.file = TFile(fileName, "RECREATE")
                    self.oldFilename = fileName
                    self.t = TTree('t', 'tree with histos')
                    self.t.Branch('FWD_0', self.FWD_0, 'timestamp/D:doseRate/D')
                    self.t.Branch('FWD_1', self.FWD_1, 'timestamp/D:doseRate/D')
                    self.t.Branch('FWD_2', self.FWD_2, 'timestamp/D:doseRate/D')
                    self.t.Branch('FWD_3', self.FWD_3, 'timestamp/D:doseRate/D')
                    # ntuple = TNtuple("ntuple", "data from diamond file", "y:z")
                    print("Created new File " + fileName)
                parts = line.split()
                diamondName = parts[0]
                data = parts[1]
                hour = parts[2]
                doseRate = float(parts[3])
                space = " "
                dataTime = data + space + hour[0:15]

                unixtime_milliseconds = self.getUnixTime(dataTime)

                self.checkRepeat(diamondName, "FWD:Ch0", self.fwd0_enter)
                self.checkRepeat(diamondName, "FWD:Ch1", self.fwd1_enter)
                self.checkRepeat(diamondName, "FWD:Ch2", self.fwd2_enter)
                self.checkRepeat(diamondName, "FWD:Ch3", self.fwd3_enter)

                if "FWD:Ch0" in diamondName:
                    self.fwd0_enter = True
                    self.FWD_0.timestamp = unixtime_milliseconds
                    self.FWD_0.doseRate = doseRate
                    fwd0Entries = fwd0Entries + 1
                    # print(" timestamp ch0= " + str(unixtime_milliseconds))
                    # print(" doseRate ch0= " + str(doseRate))
                elif "FWD:Ch1" in diamondName:
                    self.fwd1_enter = True
                    self.FWD_1.timestamp = unixtime_milliseconds
                    self.FWD_1.doseRate = doseRate
                    # print(" timestamp ch1= " + str(unixtime_milliseconds))
                    # print(" doseRate ch1= " + str(doseRate))
                elif "FWD:Ch2" in diamondName:
                    self.fwd2_enter = True
                    self.FWD_2.timestamp = unixtime_milliseconds
                    self.FWD_2.doseRate = doseRate
                    # print(" timestamp ch2= " + str(unixtime_milliseconds))
                    # print(" doseRate ch2= " + str(doseRate))
                elif "FWD:Ch3" in diamondName:
                    self.fwd3_enter = True
                    self.FWD_3.timestamp = unixtime_milliseconds
                    self.FWD_3.doseRate = doseRate
                    # print(" timestamp ch3= " + str(unixtime_milliseconds))
                    # print(" doseRate ch3= " + str(doseRate))

                if (self.fwd0_enter and self.fwd1_enter and self.fwd2_enter and self.fwd3_enter):
                    self.push()
        print("Parsed entries:" + str(fwd0Entries))
        print("Missed entries:" + str(self.missedEntries))
        self.file.Write()
        self.file.Close()

    def push(self):
        self.t.Fill()
        self.fwd0_enter = False
        self.fwd1_enter = False
        self.fwd2_enter = False
        self.fwd3_enter = False
        self.FWD_0.doseRate = float('nan')
        self.FWD_1.doseRate = float('nan')
        self.FWD_2.doseRate = float('nan')
        self.FWD_3.doseRate = float('nan')
        self.FWD_0.timestamp = float('nan')
        self.FWD_1.timestamp = float('nan')
        self.FWD_2.timestamp = float('nan')
        self.FWD_3.timestamp = float('nan')

    def checkRepeat(self, diamondName, channelName, alreadyEnter):
        if alreadyEnter and channelName in diamondName:
            self.missedEntries = self.missedEntries + 1
            self.push()

    def getUnixTime(self, dataTime):
        timeStamp_naive = datetime.strptime(dataTime, "%Y-%m-%d %H:%M:%S.%f")
        timeStamp_jst = timeStamp_naive.replace(tzinfo=timezone(FILE_TIME_ZONE))
        timeStamp_local = timeStamp_jst.astimezone(timezone(LOCAL_TIME_ZONE))
        unixtime = float(time.mktime(timeStamp_local.timetuple()))
        milliseconds = float(timeStamp_local.microsecond) / 1000000
        unixtime_milliseconds = unixtime + milliseconds
        # print("JST: " + str(timeStamp_jst))
        # print("Rome: " + str(timeStamp_local))
        # print("Unix Time: " + str(unixtime_milliseconds))
        return unixtime_milliseconds


def main():
    source = sys.stdin
    if len(sys.argv) > 1:
        source = open(sys.argv[1], "r")
    createFile = CreateDailyRootFile(source)
    createFile.readStream()


if __name__ == "__main__":
    main()
