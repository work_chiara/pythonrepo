import getopt
import sys


def main():
    # Manage arguments
    global start, end
    try:
        opts, args = getopt.getopt(sys.argv[1:], "s:e:", [])
    except getopt.GetoptError:
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-s':
            start = arg
        elif opt == '-e':
            end = arg

    print("Start= " + start)
    print("End= " + end)

    # Legge la pipline di ingresso (e per ogni riga la stampa)
    for line in sys.stdin:
        print(line)


if __name__ == "__main__":
    main()

