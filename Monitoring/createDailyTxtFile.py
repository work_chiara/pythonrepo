import sys
import re

oldFilename = ""
file=None

for line in sys.stdin:
    if "WD:Ch" in line:
        parts = line.split()
        newData=parts[1]
        fileName="BEAST_II-diamondDoseRate-"+newData+".txt"
        if fileName!=oldFilename:
            if file is not None:
                print("Closed File "+ oldFilename)
                oldFilename=fileName
                file.close()
            file = open(fileName,'a+')
            print("Created new File "+ fileName)
        file.write(line)
file.close()
