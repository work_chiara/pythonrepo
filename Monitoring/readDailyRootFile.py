import math
import sys
import re
import time
from array import array
from datetime import datetime
from pytz import timezone, all_timezones
from ROOT import TFile, TNtuple, TTree, gROOT, AddressOf

oldFilename = ""
file = None
t = None

gROOT.ProcessLine(
    "struct MyStruct {\
        Double_t   timestamp;\
        Double_t     doseRate;\
    };");

from ROOT import MyStruct

FWD_1 = MyStruct()
timeStamp_vec = list()
doseRate_vec0 = array('d')

f = TFile.Open(sys.argv[1])

if (f.IsZombie()):
    print("Erroro reading the file")
    exit(-1)

tree = f.Get('t')

FWD_0 = MyStruct()

tree.SetBranchAddress("FWD_1", AddressOf(FWD_1, 'timestamp'))
for i in range(tree.GetEntries()):
    tree.GetEntry(i)
    # print ("%.9f" % FWD_0.timestamp)
    # print("%.9f" % FWD_0.doseRate)
    if math.isnan(FWD_1.timestamp) == False:
        timeStamp = datetime.fromtimestamp(FWD_1.timestamp, tz=timezone('Asia/Tokyo'))
        doseRate_vec0.append(FWD_1.doseRate)
        timeStamp_vec.append(timeStamp)

integratedDose = 0
for i in range(len(doseRate_vec0) - 1):
    # print("%.9f" % doseRate_vec0[i])
    thres = 0.0028
    if doseRate_vec0[i] > thres:
        diff = timeStamp_vec[i + 1] - timeStamp_vec[i]
        integratedDose = integratedDose + doseRate_vec0[i] * diff.total_seconds()
print(integratedDose)
print(len(doseRate_vec0))
